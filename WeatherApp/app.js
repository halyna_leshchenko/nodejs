const express=require("express");
let app = express();
const fetch=require("node-fetch");
app.set('view engine', 'hbs');
const hbs = require('hbs');
hbs.registerPartials(__dirname + '/views/partials');

let key='c6c291361d72a6b0cce9d41ef70b464b';

app.get('/', (req, res) => {
    res.render('home.hbs');
});

app.get('/weather', (req, res) => {
    res.redirect('/weather/Zhytomyr');
});



app.get('/weather(/:city?)', async (req, res)=>{

    let city = req.params.city;
    if(!city){
        city = req.query.city;
    }
    if(!city){
        res.render("400")
        return;
    }
    let key='c6c291361d72a6b0cce9d41ef70b464b';
    let url = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${key}&units=metric`
    let response = await fetch(url);
    let weather = await response.json();

    res.render('weather', {city, weather});
});


//task_5
// app.get('/weather', (req, res)=>{
//     const weather = {
//         description:"Clear sky"
//     }
//     res.render('weather.hbs');
// });


//task 3
// app.get('/weather', (req, res)=>{
//     if(req.query.city){
//         res.send("Weather information for city: " + req.query.city);
//     } else {
//         res.send("This is a Weather Page");
//     }
// });

// app.get('/weather/:city', (req, res)=>{ // Семантичний URL
//     res.send("Weather information for city: " + req.params.city);
// });



app.listen(3000, ()=>{
        console.log("Example app listening on port 3000");
})

