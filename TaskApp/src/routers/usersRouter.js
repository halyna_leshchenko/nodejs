const express = require('express');
const router = express.Router();
const User = require('../models/user'); // Імпорт моделі User

// Створення нового користувача
router.post('/users', async (req, res) => {
    const { name, age, password, email } = req.body;
    try {
        const user = new User({ name, age, password, email });
        await user.save();
        res.status(201).send(user);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Отримання всіх користувачів
router.get('/users/', async (req, res) => {
    try {
        const users = await User.find();
        res.status(200).send(users);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Отримання користувача за ID
router.get('/users/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const user = await User.findById(id);
        if (!user) {
            return res.status(404).send('User not found');
        }
        res.status(200).send(user);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Оновлення користувача за ID
router.patch('/users/:id', async (req, res) => {
    const { id } = req.params;
    const { name, age, email } = req.body;
    try {
        const user = await User.findById(id);
        if (!user) {
            return res.status(404).send('User not found');
        }
        user.name = name;
        user.age = age;
        user.email = email;
        await user.save();
        res.status(200).send(user);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Видалення користувача за ID
router.delete('/users/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const user = await User.findByIdAndDelete(id);
        if (!user) {
            return res.status(404).send('User not found');
        }
        res.status(200).send('User deleted');
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Видалення всіх користувачів
router.delete('/users', async (req, res) => {
    try {
        await User.deleteMany();
        res.status(200).send('All users deleted');
    } catch (error) {
        res.status(500).send(error.message);
    }
});

module.exports = router;
