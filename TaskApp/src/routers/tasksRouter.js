const express = require('express');
const router = express.Router();
const Task = require('../models/task'); // Імпорт моделі Task

// Створення нового завдання
router.post('/tasks', async (req, res) => {
    const { title, description } = req.body;
    try {
        const task = new Task({ title, description });
        await task.save();
        res.status(201).send(task);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Отримання всіх завдань
router.get('/tasks', async (req, res) => {
    try {
        const tasks = await Task.find();
        res.status(200).send(tasks);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Отримання завдання за ID
router.get('/tasks/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const task = await Task.findById(id);
        if (!task) {
            return res.status(404).send('Task not found');
        }
        res.status(200).send(task);
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Оновлення завдання за ID
router.patch('/tasks/:id', async (req, res) => {
    const { id } = req.params;
    const { title, description } = req.body;
    try {
        const task = await Task.findById(id);
        if (!task) {
            return res.status(404).send('Task not found');
        }
        task.title = title;
        task.description = description;
        await task.save();
        res.status(200).send(task);
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Видалення завдання за ID
router.delete('/tasks/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const task = await Task.findByIdAndDelete(id);
        if (!task) {
            return res.status(404).send('Task not found');
        }
        res.status(200).send('Task deleted');
    } catch (error) {
        res.status(500).send(error.message);
    }
});

// Видалення всіх завдань
router.delete('/tasks/', async (req, res) => {
    try {
        await Task.deleteMany();
        res.status(200).send('All tasks deleted');
    } catch (error) {
        res.status(500).send(error.message);
    }
});

module.exports = router;
