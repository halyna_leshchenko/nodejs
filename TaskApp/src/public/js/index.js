//Створення користувача, обробник подій і асинхронна функція:
document.getElementById('userForm').addEventListener('submit', function(event) {
    event.preventDefault();

    const name = document.getElementById('name').value;
    const age = document.getElementById('age').value;
    const password = document.getElementById('password').value;
    const email = document.getElementById('email').value;

    const userData = { name, age, password, email };
    createUser(userData);
});
function createUser(userData) {
    console.log('Creating user with data:', userData);

    fetch('/users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(userData)
    })
        .then(response => {
            console.log('Response received:', response);
            if (!response.ok) {
                throw new Error('Failed to create user');
            }
            return response.json();
        })
        .then(data => {
            console.log('User created:', data);
        })
        .catch(error => {
            console.error('Error creating user:', error);
        });
}

//Діставання всіх юзерів
document.getElementById('loadUsersButton').addEventListener('click', function() {
    loadUsers()
        .then(users => {
            const userList = document.getElementById('userList');
            userList.innerHTML = "";
            users.forEach(user => {
                const li = document.createElement("li");
                li.textContent = `${user.name} - Age: ${user.age} - Email: ${user.email}`;
                userList.appendChild(li);
            });
        })
        .catch(error => {
            console.error("Error loading users:", error);
        });
});

async function loadUsers() {
    try {
        const response = await fetch('/users');
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        const data = await response.json();
        if (Array.isArray(data) && data.length > 0) {
            return data; // Повертаємо масив користувачів
        } else {
            throw new Error('Data is not in the expected format or is empty');
        }
    } catch (error) {
        console.error('Error loading users:', error);
        throw error;
    }
}


//Дістаємо користувача за id:
document.getElementById('loadUserByIdButton').addEventListener("click", function() {
    const userId = prompt("Enter user ID:");
    if (userId) {
        loadUserById(userId)
            .then(user => {
                alert(`User found: ${user.name} - Age: ${user.age} - Email: ${user.email}`);
            })
            .catch(error => {
                console.error("Error loading user:", error);
            });
    }
});

async function loadUserById(userId) {
    try {
        const response = await fetch(`/users/${userId}`);
        if (!response.ok) {
            throw new Error('Failed to load user');
        }
        const user = await response.json();
        const userDetails = document.getElementById('userDetails');
        userDetails.innerHTML = '';
        const listItem = document.createElement('li');
        listItem.textContent = `Name: ${user.name}, Age: ${user.age}, Email: ${user.email}`;
        userDetails.appendChild(listItem);
        return user; // Повертаємо користувача
    } catch (error) {
        alert('Error loading user');
        console.error('Error loading user:', error.message);
        throw error;
    }
}


//Редагування користувача:
document.getElementById('editUserByIdButton').addEventListener("click", function() {
    const userId = prompt("Enter user ID:");
    if (userId) {
        loadUserById(userId)
            .then(user => {
                const editForm = document.getElementById("editForm");
                editForm.style.display = "block";
                document.getElementById("editUserId").value = user._id;
                document.getElementById("editUserName").value = user.name;
                document.getElementById("editUserAge").value = user.age;
                document.getElementById("editUserEmail").value = user.email;
            })
            .catch(error => {
                console.error("Error loading user:", error);
            });
    }
});
async function updateUser() {
    try {
        const userId = document.getElementById('editUserId').value;
        const name = document.getElementById('editUserName').value;
        const age = document.getElementById('editUserAge').value;
        const email = document.getElementById('editUserEmail').value;

        const userData = { name, age, email };

        const response = await fetch(`/users/${userId}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userData)
        });

        if (!response.ok) {
            throw new Error('Failed to update user');
        }

        const updatedUser = await response.json();
        console.log('User updated:', updatedUser);

    } catch (error) {
        console.error('Error updating user:', error.message);
    }
}

document.getElementById('editForm').addEventListener("submit", function(event) {
    event.preventDefault();
    const userId = document.getElementById("editUserId").value;
    const updatedUserData = {
        name: document.getElementById("editUserName").value,
        age: document.getElementById("editUserAge").value,
        email: document.getElementById("editUserEmail").value
    };
    updateUser(userId, updatedUserData)
        .then(updatedUser => {
            alert("User updated successfully!");
            document.getElementById("editForm").style.display = "none";
            document.getElementById("editForm").reset();
        })
        .catch(error => {
            console.error("Error updating user:", error);
        });
});

//Видалення користувача по id:
document.getElementById('deleteUserForm').addEventListener("submit", function(event) {
    event.preventDefault();
    const userId = document.getElementById("deleteUserId").value;
    deleteUser(userId)
        .then(() => {
            alert("User deleted successfully!");
            document.getElementById("deleteUserForm").reset();
        })
        .catch(error => {
            console.error("Error deleting user:", error);
        });
});
async function deleteUser() {
    try {
        const userId = document.getElementById('deleteUserId').value;
        const response = await fetch(`/users/${userId}`, {
            method: 'DELETE'
        });

        if (!response.ok) {
            throw new Error('Failed to delete user');
        }

        console.log('User deleted successfully');

    } catch (error) {
        console.error('Error deleting user:', error.message);
    }
}
//Видалення всіх користувачів:
document.getElementById('deleteAllUsersButton').addEventListener("click", function() {
    deleteAllUsers()
        .then(() => {
            alert("All users deleted successfully!");
            document.getElementById('userList').innerHTML = "";
        })
        .catch(error => {
            console.error("Error deleting all users:", error);
        });
});
async function deleteAllUsers() {
    try {
        const response = await fetch('/users', {
            method: 'DELETE'
        });

        if (!response.ok) {
            throw new Error('Failed to delete all users');
        }

        console.log('All users deleted successfully');

    } catch (error) {
        console.error('Error deleting all users:', error.message);
    }
}

// Створення таска
document.addEventListener('DOMContentLoaded', function() {
    const taskForm = document.getElementById('taskForm');

    taskForm.addEventListener("submit", function(event) {
        event.preventDefault();
        const formData = new FormData(taskForm);
        const taskData = {
            title: formData.get("taskName"),
            description: formData.get("taskDescription")
        };
        createTask(taskData)
            .then(task => {
                alert("Task created successfully!");
                taskForm.reset();
            })
            .catch(error => {
                console.error("Error creating task:", error);
            });
    });
});

async function createTask(taskData) {
    try {
        const response = await fetch('/tasks', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(taskData)
        });

        if (!response.ok) {
            throw new Error('Failed to create task');
        }

        const newTask = await response.json();
        console.log('Task created:', newTask);
        return newTask;

    } catch (error) {
        console.error('Error creating task:', error.message);
    }
}


//Виведення всіх тасків
document.getElementById('loadTasksButton').addEventListener("click", function() {
    loadTasks()
        .then(tasks => {
            const taskList = document.getElementById('taskList');
            taskList.innerHTML = "";
            tasks.forEach(task => {
                const li = document.createElement("li");
                li.textContent = `${task.title} - ${task.description}`;
                taskList.appendChild(li);
            });
        })
        .catch(error => {
            console.error("Error loading tasks:", error);
        });
});

async function loadTasks() {
    try {
        const response = await fetch('/tasks');
        if (!response.ok) {
            throw new Error('Failed to load tasks');
        }
        const tasks = await response.json();

        console.log('Loaded tasks:', tasks);

        if (!Array.isArray(tasks)) {
            throw new Error('Tasks is not an array');
        }

        return tasks;

    } catch (error) {
        console.error('Error loading tasks:', error.message);
        throw error;
    }
}

//Виведення таска за id
document.getElementById('loadTaskByIdButton').addEventListener("click", function() {
    const taskId = prompt("Enter task ID:");
    if (taskId) {
        loadTaskById(taskId)
            .then(task => {
                if (task) {
                    alert(`Task found: ${task.title} - ${task.description}`);
                } else {
                    alert('Task not found');
                }
            })
            .catch(error => {
                console.error("Error loading task:", error);
            });
    }
});

async function loadTaskById(taskId) {
    try {
        const response = await fetch(`/tasks/${taskId}`);
        if (!response.ok) {
            throw new Error('Failed to load task');
        }
        const task = await response.json();

        if (!task.title || !task.description) {
            throw new Error('Invalid task data');
        }

        return task;

    } catch (error) {
        console.error('Error loading task:', error.message);
        throw error;
    }
}


//Редагування таска за id:
document.getElementById('editTaskByIdButton').addEventListener("click", function() {
    const taskId = prompt("Enter task ID:");
    if (taskId) {
        loadTaskById(taskId)
            .then(task => {
                const editTaskForm = document.getElementById("editTaskForm");
                editTaskForm.style.display = "block";
                document.getElementById("editTaskId").value = task._id;
                document.getElementById("editTaskName").value = task.title;
                document.getElementById("editTaskDescription").value = task.description;
            })
            .catch(error => {
                console.error("Error loading task:", error);
            });
    }
});
async function updateTask() {
    const taskId = document.getElementById('editTaskId').value;
    const taskName = document.getElementById('editTaskName').value;
    const taskDescription = document.getElementById('editTaskDescription').value;

    const updatedTask = {
        title: taskName,
        description: taskDescription
    };

    try {
        const response = await fetch(`/tasks/${taskId}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(updatedTask)
        });

        if (!response.ok) {
            throw new Error('Error updating task');
        }

        const updatedTaskData = await response.json();
        console.log('Task updated:', updatedTaskData);

    } catch (error) {
        console.error('Error updating task:', error.message);
    }
}
document.getElementById('editTaskForm').addEventListener("submit", function(event) {
    event.preventDefault();
    const taskId = document.getElementById("editTaskId").value;
    const updatedTaskData = {
        name: document.getElementById("editTaskName").value,
        description: document.getElementById("editTaskDescription").value
    };
    updateTask(taskId, updatedTaskData)
        .then(updatedTask => {
            alert("Task updated successfully!");
            document.getElementById("editTaskForm").style.display = "none";
            document.getElementById("editTaskForm").reset();
        })
        .catch(error => {
            console.error("Error updating task:", error);
        });
});
//Видалення таска
document.getElementById('deleteTaskForm').addEventListener("submit", function(event) {
    event.preventDefault();
    const taskId = document.getElementById("deleteTaskId").value;
    deleteTask(taskId)
        .then(() => {
            alert("Task deleted successfully!");
            document.getElementById("deleteTaskForm").reset();
        })
        .catch(error => {
            console.error("Error deleting task:", error);
        });
});
async function deleteTask() {
    const taskId = document.getElementById('deleteTaskId').value;

    try {
        const response = await fetch(`/tasks/${taskId}`, {
            method: 'DELETE'
        });

        if (!response.ok) {
            throw new Error('Error deleting task');
        }

        console.log('Task deleted successfully');

    } catch (error) {
        console.error('Error deleting task:', error.message);
    }
}

//Видалення всіх тасків
document.getElementById('deleteAllTasksButton').addEventListener("click", function() {
    deleteAllTasks()
        .then(() => {
            alert("All tasks deleted successfully!");
            document.getElementById('taskList').innerHTML = "";
        })
        .catch(error => {
            console.error("Error deleting all tasks:", error);
        });
});

async function deleteAllTasks() {
    try {
        const response = await fetch('/tasks', {
            method: 'DELETE'
        });

        if (!response.ok) {
            throw new Error('Error deleting all tasks');
        }

        console.log('All tasks deleted successfully');

    } catch (error) {
        console.error('Error deleting all tasks:', error.message);
    }
}






