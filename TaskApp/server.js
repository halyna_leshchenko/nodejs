const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
require('dotenv').config();

const app = express();
const PORT = process.env.PORT || 3000;
const MONGO_URL = process.env.MONGO_URL;

const usersRouter = require('./src/routers/usersRouter');
const tasksRouter = require('./src/routers/tasksRouter');

app.use(express.json());
app.use(express.static(path.join(__dirname, 'src', 'public')));

app.use( usersRouter);
app.use(tasksRouter);

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'src', 'public', 'index.html'));
});

const connectToDb = async () => {
    try {
        await mongoose.connect(MONGO_URL);
        console.log('MongoDB connected');
    } catch (err) {
        console.log(`DB connection error: ${err}`);
        process.exit(1);
    }
};

connectToDb().then(() => {
    app.listen(PORT, () => {
        console.log(`Listening on port ${PORT}`);
    });
});
