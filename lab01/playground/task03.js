const os = require('os');

// Отримуємо ім'я користувача операційної системи, в мене це galin
const userName = os.userInfo().username;

// Рядок з привітанням
const greeting = `Hello, ${userName}!`;

// Виводимо привітання на консоль
console.log(greeting);