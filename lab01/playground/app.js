// app.js

const yargs = require('yargs');
const user = require('./user.js');

yargs.command({
    command: 'add',
    describe: 'Add a language to the user',
    builder: {
        title: {
            describe: 'Language title',
            demandOption: true,
            type: 'string'
        },
        level: {
            describe: 'Language level (Beginner, Intermediate, Advanced)',
            demandOption: true,
            type: 'string'
        }
    },
    handler: (argv) => {
        const language = { title: argv.title, level: argv.level };
        user.addUserLanguage(language);
    }
});

yargs.command({
    command: 'remove',
    describe: 'Remove a language from the user',
    builder: {
        title: {
            describe: 'Language title',
            demandOption: true,
            type: 'string'
        }
    },
    handler: (argv) => {
        user.removeUserLanguage(argv.title);
    }
});

yargs.command({
    command: 'list',
    describe: 'List all languages of the user',
    handler: () => {
        user.listUserLanguages();
    }
});

yargs.command({
    command: 'read <title>',
    describe: 'Read information about a specific language',
    handler: (argv) => {
        user.readLanguageInfo(argv.title);
    }
});

yargs.parse();
