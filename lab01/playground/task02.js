const fs = require('fs');

const newLine = "Hello, world!";
const fileName = 'task02.txt';

fs.appendFile(fileName, newLine + '\n', function (err) {
    if (err) {
        console.error('Помилка при записі до файлу:', err);
        return;
    }
    else {
        console.log('Рядок успішно додано до файлу.');
        return 0;
    }
});
