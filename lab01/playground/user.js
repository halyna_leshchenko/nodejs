
const fs = require('fs');

const loadUser = () => {
    try {
        const userData = fs.readFileSync('user.json', 'utf8');
        return JSON.parse(userData);
    } catch (error) {
        console.error("Error reading user data:", error);
        return null;
    }
};

const saveUser = (userData) => {
    try {
        fs.writeFileSync('user.json', JSON.stringify(userData, null, 2));
        console.log("User data updated successfully.");
    } catch (error) {
        console.error("Error saving user data:", error);
    }
};

const addUserLanguage = (language) => {
    const user = loadUser();
    if (user) {
        const existingLanguage = user.languages.find(lang => lang.title === language.title);
        if (!existingLanguage) {
            user.languages.push(language);
            saveUser(user);
        } else {
            console.log("Language already exists for the user.");
        }
    }
};

const removeUserLanguage = (languageTitle) => {
    const user = loadUser();
    if (user) {
        const index = user.languages.findIndex(lang => lang.title === languageTitle);
        if (index !== -1) {
            user.languages.splice(index, 1);
            saveUser(user);
        } else {
            console.log("Language not found for the user.");
        }
    }
};

const listUserLanguages = () => {
    const user = loadUser();
    if (user) {
        console.log("User languages:");
        user.languages.forEach(lang => console.log(`Title: ${lang.title}, Level: ${lang.level}`));
    }
};

const readLanguageInfo = (title) => {
    const user = loadUser();
    if (user) {
        const language = user.languages.find(lang => lang.title === title);
        if (language) {
            console.log(`Language: ${language.title}`);
            console.log(`Level: ${language.level}`);
        } else {
            console.log("Language not found for the user.");
        }
    }
};

module.exports = {
    addUserLanguage,
    removeUserLanguage,
    listUserLanguages,
    readLanguageInfo
};
