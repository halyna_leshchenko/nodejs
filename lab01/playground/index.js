// Підключаємо модуль Lodash
const _ = require('lodash');
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Введіть дані через пробіл: ', (inputString) => {
    // Отримуємо дані з введення користувача та розділяємо їх на масив
    const userInputData = inputString.split(' ');

    // Додаємо дані користувача до process.argv
    process.argv.push(...userInputData);

    // Метод 1: _.chunk()
    // Розділяє вхідний масив на групи заданого розміру
    const chunkedArray = _.chunk(userInputData, 2);
    console.log('Метод 1 (chunk):', chunkedArray);

    // Метод 2: _.sortBy()
    // Сортує масив за заданою властивістю
    const sortedArray = _.sortBy(userInputData);
    console.log('Метод 2 (sortBy):', sortedArray);

    // Метод 3: _.filter()
    // Фільтрує масив за певною умовою
    const filteredArray = _.filter(userInputData, item => item.length > 5);
    console.log('Метод 3 (filter):', filteredArray);

    // Метод 4: _.map()
    // Застосовує задану функцію до кожного елементу масиву
    const mappedArray = _.map(userInputData, item => item.toUpperCase());
    console.log('Метод 4 (map):', mappedArray);

    // Метод 5: _.uniq()
    // Повертає масив з унікальними значеннями
    const uniqueArray = _.uniq(userInputData);
    console.log('Метод 5 (uniq):', uniqueArray);
    //Виводить дані з process.argv
    console.log('process.argv',process.argv.slice(2));

    rl.close();
});
